# Vaatimukset
Ohjelma vaatii MySQL-palvelimen saataville
`backend/.env`-tiedostossa määritettyyn osoitteeseen.
Myös muut tiedostossa olevat asetukset tulee määrittää.

Tietokannan taulut voi luoda suorittamalla komennon
`diesel migration run` hakemistossa `backend`.

# Ohjelman käynnistäminen
Käännä ensin asiakaspuoli WebAssemblyksi suorittamalla
`frontend`-hakemistossa `cargo web deploy`. Tämän
jälkeen voit siirtyä hakemistoon `backend`, jossa
palvelimen voi käynnistää komennolla `cargo run`.
Palvelin tulostaa terminaaliin mitä osoitetta se kuuntelee.
