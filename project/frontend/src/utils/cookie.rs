use anyhow::Result;
use log::info;
use stdweb::{js, unstable::TryInto};
use thiserror::Error;

#[derive(Debug, Error)]
pub enum CookieError {
    #[error("No cookie found for given name")]
    NotFound,
}

pub fn get(name: &str) -> Result<String> {
    let cookie_strings = js! { return document.cookie };
    let cookies: Vec<String> = cookie_strings.try_into()?;
    cookies
        .iter()
        .filter_map(|x| {
            let name_value: Vec<_> = x.split("=").collect();
            match name_value.get(0) {
                None => None,
                Some(c) => {
                    if *c == name {
                        name_value.get(1).map(|x| (*x).to_owned())
                    } else {
                        None
                    }
                }
            }
        })
        .collect::<Vec<String>>()
        .pop()
        .ok_or_else(|| CookieError::NotFound.into())
}
