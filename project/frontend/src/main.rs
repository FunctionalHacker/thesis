#![recursion_limit = "256"]

extern crate log;
extern crate web_logger;

mod component;
pub mod utils;

use component::{login::LoginComponent, protected::ProtectedComponent};
use yew::prelude::*;
use yew::virtual_dom::VNode;
use yew_router::{prelude::*, switch::Permissive, Switch};

struct App {}

#[derive(Debug, Switch, Clone)]
enum AppRoute {
    #[to = "/login"]
    Login,
    #[to = "/"]
    Root,
    PageNotFound(Permissive<String>),
}

impl Component for App {
    type Message = ();
    type Properties = ();

    fn create(_: Self::Properties, _link: ComponentLink<Self>) -> Self {
        App {}
    }

    fn update(&mut self, _msg: Self::Message) -> ShouldRender {
        true
    }

    fn view(&self) -> VNode {
        html! {
            <div>
                <nav class="menu",>
                    <RouterButton<AppRoute> route=AppRoute::Login> {"Kirjautuminen"} </RouterButton<AppRoute>>
                    <RouterButton<AppRoute> route=AppRoute::Root> {"Suojattu data"} </RouterButton<AppRoute>>
                </nav>
                <Router<AppRoute>
                    render = Router::render(|switch: AppRoute| {
                        match switch {
                            AppRoute::Login => html!{<LoginComponent />},
                            AppRoute::PageNotFound(Permissive(None)) => html!{"Page not found"},
                            AppRoute::PageNotFound(Permissive(Some(missed_route))) => html!{format!("Page '{}' not found", missed_route)},
                            AppRoute::Root => {
                                html!{<ProtectedComponent />}
                            },
                        }
                    })
                    redirect = Router::redirect(|route: Route| {
                        AppRoute::PageNotFound(Permissive(Some(route.route)))
                    })
                />
            </div>
        }
    }
}

fn main() {
    web_logger::init();
    yew::start_app::<App>();
}
