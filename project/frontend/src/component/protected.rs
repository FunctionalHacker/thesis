use log::{error, info};
use yew::format::Nothing;
use yew::prelude::*;
use yew::services::fetch::{FetchService, FetchTask, Request, Response};

pub struct ProtectedComponent {
    component_link: ComponentLink<ProtectedComponent>,
    fetch_service: FetchService,
    fetch_task: Option<FetchTask>,
    fetching: bool,
    data: String,
}

pub enum Msg {
    FetchData(),
    FetchReady(String),
    FetchError,
}

impl ProtectedComponent {
    fn get_data(&mut self) {
        self.fetching = true;

        let request = Request::get("http://localhost:3880/api/protected")
            .body(Nothing)
            .unwrap();

        info!("Request: {:?}", request);

        let callback =
            self.component_link
                .callback(|response: Response<Result<String, anyhow::Error>>| {
                    let (meta, body) = response.into_parts();
                    info!("{}", meta.status);
                    if meta.status.is_success() {
                        Msg::FetchReady(body.unwrap())
                    } else {
                        Msg::FetchError
                    }
                });

        let task = self.fetch_service.fetch(request, callback);
        self.fetch_task = Some(task.unwrap());
    }
}

impl Component for ProtectedComponent {
    type Message = Msg;
    type Properties = ();

    fn create(_: Self::Properties, link: ComponentLink<Self>) -> Self {
        Self {
            component_link: link,
            fetch_service: FetchService::new(),
            fetch_task: None,
            fetching: false,
            data: String::from(""),
        }
    }

    fn change(&mut self, _: Self::Properties) -> ShouldRender {
        true
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::FetchReady(response) => {
                self.fetching = false;
                info!("Fetch successful: {}", response);
                self.data = response;
            }

            Msg::FetchError => {
                self.fetching = false;
                error!("There was an error connecting to API");
                self.data = String::from("401 Unauthorized");
            }

            Msg::FetchData() => {
                self.fetching = true;
                self.get_data();
            }
        }
        true
    }

    fn view(&self) -> Html {
        let onclick = self.component_link.callback(|_| Msg::FetchData());

        html! {
            <div class="uk-card uk-card-default uk-card-body uk-width-1-3@s uk-position-center">
                <h1 class="uk-card-title">{ "Suojattu data" }</h1>
                <p>{&self.data}</p>
                <button
                class="uk-button uk-button-primary",
                type="button",
                disabled=self.fetching,
                onclick=onclick>
                    { "Hae data" }
                </button>
            </div>
        }
    }
}
