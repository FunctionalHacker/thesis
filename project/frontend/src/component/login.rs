use log::{error, info};
use serde_json::json;
use yew::format::Json;
use yew::prelude::*;
use yew::services::fetch::{FetchService, FetchTask, Request, Response};

pub struct LoginComponent {
    component_link: ComponentLink<LoginComponent>,
    username: String,
    password: String,
    login_button_disabled: bool,
    fetch_service: FetchService,
    fetch_task: Option<FetchTask>,
    fetching: bool,
}

pub enum Msg {
    UpdateUsername(String),
    UpdatePassword(String),
    HandleForm(),
    FetchReady(String),
    FetchError,
}

impl LoginComponent {
    fn update_button_state(&mut self) {
        self.login_button_disabled = self.username.is_empty() || self.password.is_empty();
    }

    fn post_login(&mut self) {
        self.fetching = true;

        let user = json!({
            "username": self.username,
            "password": self.password
        });

        let request = Request::builder()
            .method("POST")
            .header("Content-Type", "application/json")
            .uri("http://localhost:3880/api/auth/login")
            .body(Json(&user))
            .unwrap();

        let callback =
            self.component_link
                .callback(|response: Response<Result<String, anyhow::Error>>| {
                    let (meta, body) = response.into_parts();
                    if meta.status.is_success() {
                        Msg::FetchReady(body.unwrap())
                    } else {
                        Msg::FetchError
                    }
                });

        let task = self.fetch_service.fetch(request, callback);
        self.fetch_task = Some(task.unwrap());
    }
}

impl Component for LoginComponent {
    type Message = Msg;
    type Properties = ();

    fn create(_: Self::Properties, link: ComponentLink<Self>) -> Self {
        Self {
            component_link: link,
            username: String::new(),
            password: String::new(),
            login_button_disabled: true,
            fetch_service: FetchService::new(),
            fetch_task: None,
            fetching: false,
        }
    }

    fn change(&mut self, _: Self::Properties) -> ShouldRender {
        true
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::UpdateUsername(new_username) => {
                self.username = new_username;
                self.update_button_state();
            }

            Msg::UpdatePassword(new_password) => {
                self.password = new_password;
                self.update_button_state();
            }

            Msg::HandleForm() => self.post_login(),

            Msg::FetchReady(response) => {
                self.fetching = false;
                info!("Login successful: {}", response);
            }

            Msg::FetchError => {
                self.fetching = false;
                error!("There was an error connecting to API")
            }
        }
        true
    }

    fn view(&self) -> Html {
        let onclick = self.component_link.callback(|_| Msg::HandleForm());
        let oninput_username = self
            .component_link
            .callback(|e: InputData| Msg::UpdateUsername(e.value));
        let oninput_password = self
            .component_link
            .callback(|e: InputData| Msg::UpdatePassword(e.value));

        html! {
            <div class="uk-card uk-card-default uk-card-body uk-width-1-3@s uk-position-center">
                <h1 class="uk-card-title">{ "Kirjaudu sisään" }</h1>
                <div>
                    <fieldset class="uk-fieldset">
                        <input class="uk-input uk-margin",
                            placeholder="Käyttäjänimi",
                            disabled=self.fetching,
                            value=&self.username,
                            oninput=oninput_username, />
                        <input class="uk-input uk-margin-bottom",
                            placeholder="Salasana",
                            disabled=self.fetching,
                            type="password",
                            value=&self.password,
                            oninput=oninput_password, />
                        <button
                        class="uk-button uk-button-primary",
                        type="button",
                        disabled=self.fetching,
                        onclick=onclick>
                            { "Kirjaudu" }
                        </button>
                    </fieldset>
                </div>
            </div>
        }
    }
}
