use crate::{
    db_connection::DbPool,
    errors::CustomError,
    handlers::logged_user::LoggedUser,
    handlers::pool_handler,
    models::user::{AuthUser, DeleteUser, RegisterUser, User},
    utils::jwt::encode_token,
};
use actix_identity::Identity;
use actix_web::{web, HttpResponse};

pub async fn register(
    new_user: web::Json<RegisterUser>,
    pool: web::Data<DbPool>,
) -> Result<HttpResponse, HttpResponse> {
    let connection = pool_handler(pool)?;
    let register_user = new_user
        .into_inner()
        .validation()
        .map_err(|e| HttpResponse::InternalServerError().json(e.to_string()))?;

    User::create(register_user, &connection)
        .map(|_r| HttpResponse::Ok().json("User created successfully"))
        .map_err(|e| HttpResponse::InternalServerError().json(e.to_string()))
}

pub async fn delete(
    user: LoggedUser,
    user_to_delete: web::Json<DeleteUser>,
    pool: web::Data<DbPool>,
) -> Result<HttpResponse, HttpResponse> {
    if user.admin || user.username == user_to_delete.username {
        let connection = pool_handler(pool)?;
        user_to_delete.delete(&connection).map_err(|e| match e {
            CustomError::DBError(diesel::result::Error::NotFound) => {
                HttpResponse::NotFound().json(e.to_string())
            }
            _ => HttpResponse::InternalServerError().json(e.to_string()),
        })?;
        Ok(HttpResponse::Ok().json("User deleted successfully"))
    } else {
        Err(HttpResponse::Unauthorized().json("Only admins can delete users"))
    }
}

pub async fn login(
    id: Identity,
    auth_user: web::Json<AuthUser>,
    pool: web::Data<DbPool>,
) -> Result<HttpResponse, HttpResponse> {
    let connection = pool_handler(pool)?;

    let user = auth_user.login(&connection).map_err(|e| match e {
        CustomError::DBError(diesel::result::Error::NotFound) => {
            HttpResponse::NotFound().json(e.to_string())
        }
        _ => HttpResponse::InternalServerError().json(e.to_string()),
    })?;

    let token = encode_token(user.id, &user.username, user.admin).map_err(|e| match e {
        _ => HttpResponse::InternalServerError().finish(),
    })?;
    println!("Token: {}", token);

    id.remember(String::from(token));
    Ok(HttpResponse::Ok().json(user))
}

pub async fn logout(id: Identity) -> Result<HttpResponse, HttpResponse> {
    id.forget();
    Ok(HttpResponse::Ok().into())
}
