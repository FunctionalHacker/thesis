use crate::handlers::logged_user::LoggedUser;
use actix_web::HttpResponse;
use serde::Serialize;

#[derive(Serialize)]
struct SuccessMessage {
    message: String,
}

pub async fn protected_route(_user: LoggedUser) -> Result<HttpResponse, HttpResponse> {
    Ok(HttpResponse::Ok().json(SuccessMessage {
        message: String::from("Tämä on suojattu viesti palvelimelta"),
    }))
}
