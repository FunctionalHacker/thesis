use crate::utils::jwt::{decode_token, UserWithToken};
use actix_identity::Identity;
use actix_web::{dev::Payload, FromRequest, HttpRequest, HttpResponse};
use futures::future::Future;
use std::pin::Pin;

pub type LoggedUser = UserWithToken;

impl FromRequest for LoggedUser {
    type Error = HttpResponse;
    type Config = ();
    type Future = Pin<Box<dyn Future<Output = Result<UserWithToken, HttpResponse>>>>;

    fn from_request(req: &HttpRequest, payload: &mut Payload) -> Self::Future {
        let fut = Identity::from_request(req, payload);

        Box::pin(async move {
            if let Some(identity) = fut.await?.identity() {
                let user = decode_token(&identity)?;
                return Ok(user);
            };
            Err(HttpResponse::Unauthorized().finish())
        })
    }
}
