use bcrypt::BcryptError;
use diesel::result;
use std::fmt;

pub enum CustomError {
    HashError(BcryptError),
    DBError(result::Error),
    PasswordMatchError(String),
    PasswordWrong(String),
}

impl From<BcryptError> for CustomError {
    fn from(error: BcryptError) -> Self {
        CustomError::HashError(error)
    }
}

impl From<result::Error> for CustomError {
    fn from(error: result::Error) -> Self {
        CustomError::DBError(error)
    }
}

impl std::fmt::Display for CustomError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            CustomError::HashError(error) => write!(f, "{}", error),
            CustomError::DBError(error) => write!(f, "{}", error),
            CustomError::PasswordMatchError(error) => write!(f, "{}", error),
            CustomError::PasswordWrong(error) => write!(f, "{}", error),
        }
    }
}
