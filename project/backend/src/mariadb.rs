use diesel::prelude::*;
use diesel::r2d2::{self, ConnectionManager};

pub type DbPool = r2d2::Pool<ConnectionManager<DbConnection>>;
pub type PooledConnection = r2d2::Pool<ConnectionManager<MysqlConnection>>;

fn init_pool(database_url: &str) -> Result<PgPool, PoolError> {
    let manager = ConnectionManager::<MysqlConnection>::new(database_url);
    r2d2::Pool::builder().build(manager)
}

pub fn get_pool() -> DbPool {
    let connspec = std::env::var("DATABASE_URL").expect("DATABASE_URL");
    init_pool(connspec).expect("Failed to create DB pool")
}
