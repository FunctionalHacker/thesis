#[macro_use]
extern crate diesel;
extern crate dotenv;

pub mod db_connection;
pub mod errors;
pub mod handlers;
pub mod models;
pub mod schema;
pub mod utils;

use actix_cors::Cors;
use actix_files::{Files, NamedFile};
use actix_identity::{CookieIdentityPolicy, IdentityService};
use actix_web::{
    get,
    http::header,
    middleware::Logger,
    web::{delete, get, post, resource, scope},
    App, Error, HttpResponse, HttpServer,
};
use chrono::Duration;
use db_connection::get_pool;
use dotenv::dotenv;
use handlers::{authentication, protected};

pub fn get_env(var_name: &str) -> String {
    match std::env::var(&var_name) {
        Ok(var) => return var,
        Err(e) => {
            eprintln!(
                "Failed to read required environment variable: {}",
                &var_name
            );
            eprintln!("Reason: {}", e.to_string());
            panic!("Can't continue without variable");
        }
    };
}

async fn serve_index_html() -> Result<NamedFile, Error> {
    Ok(NamedFile::open("./static/index.html")?)
}

#[get("/api")]
async fn api_404() -> HttpResponse {
    HttpResponse::NotFound().finish()
}

#[get("/api/{unconfigured_routes:.*}")]
async fn api_404_unconfigured() -> HttpResponse {
    HttpResponse::NotFound().finish()
}

#[rustfmt::skip]
#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    std::env::set_var("RUST_LOG", "actix_web=debug,diesel=debug");
    env_logger::init();
    dotenv().ok();

    let bind = get_env("BIND");
    let port = get_env("PORT");
    let address = format!("{}:{}", bind, port);

    println!("Starting server at: http://{}", &address);

    HttpServer::new(move || {
        App::new()
            .wrap(Logger::default())
            .wrap(
                Cors::new()
                    .allowed_origin(get_env("ALLOWED_ORIGIN").as_str())
                    .allowed_methods(vec!["GET", "POST", "PUT", "DELETE"])
                    .allowed_headers(vec![
                        header::AUTHORIZATION,
                        header::CONTENT_TYPE,
                        header::ACCEPT,
                    ])
                    .max_age(3600)
                    .finish(),
            )
            .wrap(IdentityService::new(
                CookieIdentityPolicy::new(get_env("SECRET").as_bytes())
                    .domain(get_env("DOMAIN"))
                    .name(get_env("APP_NAME"))
                    .path("/")
                    .max_age(Duration::days(30).num_seconds())
                    .secure(false),
            ))
            .data(get_pool())
            .service(api_404)
            .service(
                scope("/api/auth")
                    .service(
                        resource("/register")
                        .route(post()
                            .to(authentication::register)
                        )
                    )
                    .service(
                        resource("/login")
                        .route(post()
                            .to(authentication::login)
                        )
                    )
                    .service(
                        resource("/logout")
                        .route(post()
                            .to(authentication::logout)
                        ))
                    .service(
                        resource("/delete")
                        .route(delete()
                            .to(authentication::delete)
                        )
                    )
            )
            .service(
                resource("/api/protected")
                .route(get()
                    .to(protected::protected_route)
                )
            )
            .service(api_404_unconfigured)
            .service(
                Files::new("/", "./static")
                .index_file("index.html")
            )
            .default_service(get().to(serve_index_html))
    })
    .bind(address)?
    .run()
    .await
}
