use crate::{errors::CustomError, schema::users};
use bcrypt::{hash, verify, DEFAULT_COST};
use chrono::{Local, NaiveDateTime};
use diesel::prelude::*;
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Serialize, Deserialize, Queryable, Insertable)]
#[table_name = "users"]
pub struct User {
    #[serde(skip)]
    pub id: i32,
    pub username: String,
    #[serde(skip)]
    pub password: String,
    pub admin: bool,
    pub created_at: NaiveDateTime,
}

// another user struct for new users since id is missing on
// creation (created by mariadb)
#[derive(Debug, Clone, Serialize, Deserialize, Insertable)]
#[table_name = "users"]
pub struct NewUser {
    pub username: String,
    pub password: String,
    pub admin: bool,
    pub created_at: NaiveDateTime,
}

impl User {
    pub fn hash_password(password: String) -> Result<String, CustomError> {
        Ok(hash(password, DEFAULT_COST)?)
    }

    pub fn create(
        register_user: RegisterUser,
        connection: &MysqlConnection,
    ) -> Result<usize, CustomError> {
        use crate::schema::users::dsl::users;

        let new_user = NewUser {
            username: register_user.username,
            admin: register_user.admin,
            password: Self::hash_password(register_user.password)?,
            created_at: Local::now().naive_local(),
        };

        Ok(diesel::insert_into(users)
            .values(new_user)
            .execute(connection)?)
    }
}

#[derive(Serialize, Deserialize)]
pub struct RegisterUser {
    pub username: String,
    pub password: String,
    pub admin: bool,
    pub password_confirmation: String,
}

impl RegisterUser {
    pub fn validation(self) -> Result<RegisterUser, CustomError> {
        let passwords_are_equal = self.password == self.password_confirmation;
        let password_not_empty = self.password.len() > 0;

        if passwords_are_equal && password_not_empty {
            Ok(self)
        } else if !passwords_are_equal {
            Err(CustomError::PasswordMatchError(
                "Password and confirmation do not match".to_string(),
            ))
        } else {
            Err(CustomError::PasswordWrong(
                "Wrong or empty password".to_string(),
            ))
        }
    }
}

#[derive(Deserialize)]
pub struct AuthUser {
    pub username: String,
    pub password: String,
}

impl AuthUser {
    pub fn login(&self, connection: &MysqlConnection) -> Result<User, CustomError> {
        use crate::schema::users::dsl::*;

        let user = users
            .filter(username.eq(&self.username))
            .first::<User>(connection)?;

        let verify_password = verify(&self.password, &user.password)
            .map_err(|_e| CustomError::PasswordWrong("Wrong password".to_string()))?;

        if verify_password {
            Ok(user)
        } else {
            Err(CustomError::PasswordWrong("Wrong password".to_string()))
        }
    }
}

#[derive(Deserialize)]
pub struct DeleteUser {
    pub username: String,
}

impl DeleteUser {
    pub fn delete(&self, connection: &MysqlConnection) -> Result<bool, CustomError> {
        use crate::schema::users::dsl::*;
        match diesel::delete(users.filter(username.eq(&self.username))).execute(connection) {
            Ok(_r) => Ok(true),
            Err(e) => Err(CustomError::DBError(e)),
        }
    }
}
