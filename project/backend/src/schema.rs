table! {
    users (id) {
        id -> Integer,
        username -> Varchar,
        password -> Varchar,
        admin -> Bool,
        created_at -> Timestamp,
    }
}
