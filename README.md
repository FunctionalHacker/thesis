# Rust web-ohjelmointikielenä

Tämän insinöörityön tavoitteena oli selvittää Rust-ohjelmointikielen
soveltuvuutta web-ohjelmointiin.

Insinöörityössä käydään läpi web-ohjelmoinnin perusteet, sekä Rustin
pääominaisuudet. Lukijalta oletetaan hyvin vähän tietämystä ja kaikki
olennaiset käsitteet käydään läpi perusteista alkaen.

Insinöörityön yhteydessä tehtiin projekti, missä palvelin- ja asiakaspuoli
toteutettiin molemmat Rustilla. Projektin kaikki osa-alueet ja kaikki käytetyt
riippuvaisuudet, sekä syyt niiden valitsemiseen on käyty läpi perusteellisesti.
Myös kehitysympäristön asentaminen ja projektin aloittaminen on käyty läpi
alusta alkaen.

Projektista kerätyn käytännön tiedon avulla on arvioitu Rustin soveltuvuutta
web-ohjelmointiin erikseen sekä asiakas- että palvelinpuolella. Lopuksi on
siirretty katsetta hieman tulevaan ja arvioitu, miten kielen soveltuvuus
tulee todennäköisesti muuttumaan tulevaisuudessa. Lopussa on myös suosituksia
Rustin sisällyttämisestä uusiin ja olemassa oleviin web-ohjelmointiprojekteihin.
