struct RegisterUser {
    username: String,
    password: String,
    admin: bool,
    password_confirmation: String,
}

#[post("/auth/register")]
fn register(new_user: actix_web::web::Json<RegisterUser>) -> Result<HttpResponse, HttpResponse> {
    register(new_user);
}
