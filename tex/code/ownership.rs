fn say_hello(name: String) {
    println!("Hello {}!", name);
}

fn main() {
    let name = String::from("Marko");
    say_hello(name);

    println!("{}", name);
}
