macro_rules! laske {
    (lisää $num1:literal ja $num2:literal) => {
        println!("{} plus {} on {}", $num1, $num2, $num1 + $num2);
    };
    (vähennä $num1:literal ja $num2:literal) => {
        println!("{} miinus {} on {}", $num1, $num2, $num1 - $num2);
    };
    (kerro $num1:literal ja $num2:literal) => {
        println!("{} kertaa {} on {}", $num1, $num2, $num1 * $num2);
    };
    (jaa $num1:literal ja $num2:literal) => {
        println!("{} jaettuna {} on {}", $num1, $num2, $num1 / $num2);
    };
}

laske!(lisää 10 ja 269);
laske!(vähennä 652 ja 3);
laske!(kerro 256 ja 2);
laske!(jaa 100 ja 50);

// Tuloste:
// 10 plus 269 on 279
// 652 miinus 3 on 649
// 256 kertaa 2 on 512
// 100 jaettuna 50 on 2
