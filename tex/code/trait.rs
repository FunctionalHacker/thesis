trait Muoto {
    fn pinta_ala(&self) -> u32;
}

struct Suorakulmio {
    x: u32,
    y: u32,
}

struct Ympyra {
    r: f64,
}

impl Muoto for Suorakulmio {
    fn pinta_ala(&self) -> u32 {
        self.x * self.y
    }
}

impl Muoto for Ympyra {
    fn pinta_ala(&self) -> u32 {
        (3.141 * self.r * self.r) as u32
    }
}

fn main() {
    let muoto1 = Suorakulmio { x: 15, y: 10 };
    let muoto2 = Ympyra { r: 8.5 };
    println!("Suorakulmion pinta-ala: {}", muoto1.pinta_ala());
    println!("Ympyrän pinta-ala: {}", muoto2.pinta_ala());
}

// Suorakulmion pinta-ala: 150
// Ympyrän pinta-ala: 226
