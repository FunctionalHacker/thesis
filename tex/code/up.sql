CREATE TABLE users (
	`id` int NOT NULL AUTO_INCREMENT,
	`username` varchar(100) UNIQUE NOT NULL,
	`password` varchar(128) NOT NULL,
	`admin` boolean NOT NULL,
	`created_at` timestamp NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
