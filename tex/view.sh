#!/bin/bash
function finish {
	killall entr
}

bibtex8 main
xelatex -shell-escape -8bit main

makeglossaries main
find chapters | entr -cp xelatex -shell-escape -8bit main &
find appendix | entr -cp xelatex -shell-escape -8bit main &
find code | entr -cp xelatex -shell-escape -8bit main &
ls biblio.bib | entr -cp bibtex8 main &
zathura >/dev/null 2>&1 main.pdf

trap finish EXIT
