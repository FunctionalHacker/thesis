\chapter{Kielet}

\section{Rust}
Rust \cite{rust:lang} on Mozillan 2010 julkaisema ohjelmointikieli. Se on hyvin suorituskykyinen järjestelmätason ohjelmointikieli, joka muistuttaa monilta osin C- ja C++ -kieliä. Rustin tarkoituksena on säilyttää näiden vanhojen kielien suorituskyky, mutta kuitenkin tarjota samalla muun muassa vahva tyypitys ja taattu turvallinen rinnakaisajo. Lisäksi tyypilliset C-kielen muistinhallintaongelmat on pyritty ratkaisemaan käytännöillä, jotka ovat samalla tehokkaita suorituskyvyn näkökulmasta, mutta myös helppoja käyttää ohjelmoijalle.

\subsection{Muistinhallinta}

Monissa korkean tason ohjelmointikielissä, esimerkiksi JavaScriptissä, on automaattinen roskienkeräys \cite{wiki:garbagecollection} (engl. garbage collector). Se on prosessi, joka siivoaa muistista käyttämättömiä tietoja ja näin ollen vapauttaa muistia. Automaattisen roskienkeräyksen ongelma on, että se itsessään käyttää järjestelmän resursseja. Lisäksi roskienkeruu on hidasta.

Automaattiselle roskienkeruulle on aikaisemmin ollut vaihtoehtona vain manuaalinen muistinhallinta, missä ohjelmoija varaa ja vapauttaa muistia tarpeen mukaan. Tämä on taas verrattuna automaattiseen roskien keruuseen melko työlästä ja virheherkkää.

Rustin yhtenä pääominaisuutena on mainostettu sen uudenlaista näkökulmaa muistinhallintaan: omistajuutta \cite{rust:ownership}. Siinä jokaisella arvolla on omistaja, ja kun omistaja menee näkyvyysalueen ulkopuolelle, niin menevät myös sen omistamat arvotkin, eli ne vapautetaan muistista. Arvojen omistajuutta voi siirtää joko pysyvästi tai väliaikaisesti lainaamalla.

\begin{code}
	\inputminted{Rust}{code/ownership.rs}
	\captionof{listing}{Omistajuus Rustissa}
	\label{code:rust:ownership}
\end{code}

Koodiesimerkin \ref{code:rust:ownership} rivillä 9 tapahtuva ''name''-arvon tulostus ei toimi, koska arvon omistajuus on siirretty funktiolle ''say\char`_hello''. Kun funktio on suoritettu, se poistuu näkyvyysalueelta ja kaikki sen omistamat arvot vapautetaan muistista. Näin ollen arvoa ''name'' ei ole enää olemassa, kun sitä yritetään käyttää rivillä 9. Koodi saadaan toimimaan pienellä muutoksella.

\clearpage

\begin{code}
	\inputminted{Rust}{code/borrow.rs}
	\captionof{listing}{Lainaus}
	\label{code:rust:borrow}
\end{code}

Koodiesimerkissä \ref{code:rust:borrow} omistajuuden siirtäminen on korvattu lainauksella. Tämä muutos pitää tehdä sekä funktion parametrien määritykseen riville 1 että funktion kutsuun riville 7. Lainauksessa arvon omistajuus säilyy nykyisellään ja lainaaja antaa itse arvon sijasta viitteen (eng. reference). Viite on osoitin, joka osoittaa samaan muistipaikkaan kuin missä alkuperäinen arvo on. Lainaaminen tehdään käyttämällä merkkiä ''\char`&''.

\subsection{Vahva tyypitys}
Rust on vahvasti tyypitetty kieli, mikä tarkoittaa sitä, että kaikkien arvojen tyypit pitää olla tiedossa ohjelman kääntämisen aikana. Tähän sisältyy myös funktioiden parametrit ja paluuarvot. Usein Rustin kääntäjä osaa päätellä (eng. inference) arvojen tyypit itse, varsinkin yksinkertaisissa tapauksissa.

\bigskip

\begin{code}
	\inputminted{Rust}{code/type-inference.rs}
	\captionof{listing}{Tyypin päättely}
	\label{code:rust:inference}
\end{code}

Niissä tapauksissa, joissa tyypille voi olla useita vaihtoehtoja, tai silloin jos arvon määrityksen yhteydessä tapahtuu konversio, ohjelmoijan tulee määrittää tyyppi.

\bigskip

\begin{code}
	\inputminted{Rust}{code/type-notation.rs}
	\captionof{listing}{Tyypin merkintä}
	\label{code:rust:type-notation}
\end{code}

Koodiesimerkissä \ref{code:rust:type-notation} valitsin arvolle ''age'' tyypin u8, koska se on pienin kokonaislukutyypeistä ja sen arvo voi olla välillä 0-255. Näin voi potentiaalisesti vähentää ohjelman muistin käyttöä. Lisäksi tyypeillä voi karkeasti rajata funktion parametrien arvojen vaihteluväliä. Esimerkiksi jos on kirjoittamassa funktiota, joka ottaa parametrina henkilön iän, u8 on hyvä valinta, koska ihmisen ikä ei voi olla negatiivinen ja yksikään ihminen tuskin elää yli 255 vuotta.

Edellä mainittu tyyppi u8 on niin kutsuttu etumerkitön kokonaisluku (eng. \textbf{u}nsigned integer). Etumerkillisissä kokonaisluvuissa (eng. signed integer) käytetään yksi bitti merkkaamaan sitä, onko luku positiivinen vai negatiivinen. Etumerkittömissä  luvuissa tätä ei tehdä, joten luku voi olla hieman isompi kuin etumerkillinen luku, mutta se ei voi olla negatiivinen.

\def\2^#1{\hbox to1.6em{$2^{#1}$\hss}}

\begin{table}[h!]
	\label{tab:integers}
	\begin{center}
		\begin{minipage}{2.5in}
			\begin{tabular}{|l|c|r|}
				\hline
				\multicolumn{3}{|c|}{\textbf{Etumerkitön}} \\
				\hline
				Tyyppi & Minimi & Maksimi \\
				\hline
				u8   & 0 & $ \2^{8} -1   $\\
				u16  & 0 & $ \2^{16}-1  $\\
				u32  & 0 & $ \2^{32}-1  $\\
				u64  & 0 & $ \2^{64}-1  $\\
				u128 & 0 & $ \2^{128}-1 $\\
				\hline
			\end{tabular}
		\end{minipage}
		\begin{minipage}{2.5in}
			\begin{tabular}{|l|c|r|}
			    \hline
			    \multicolumn{3}{|c|}{\textbf{Etumerkillinen}} \\
			    \hline
			    Tyyppi & Minimi & Maksimi \\
			    \hline
			    i8   & $ -\2^{7}   $ & $ \2^{7}-1   $\\
			    i16  & $ -\2^{15}  $ & $ \2^{15}-1  $\\
			    i32  & $ -\2^{31}  $ & $ \2^{31}-1  $\\
			    i64  & $ -\2^{63}  $ & $ \2^{63}-1  $\\
			    i128 & $ -\2^{127} $ & $ \2^{127}-1 $\\
			    \hline
			\end{tabular}
				\end{minipage}
			\end{center}
			\caption{Rustin kokonaislukutyypit ja niiden vaihteluvälit}
		\end{table}

\bigskip

Toisin kuin C- ja C++ -kielissä, Rustissa on oletuksena etumerkittömien kokonaislukujen ylivuoto pois päältä. Tämä tarkoittaa sitä, että jos esimerkiksi 8-bittisen etumerkittömän kokonaisluvun (u8) arvoksi yritetään asettaa 256, siitä tulee 0. Rustin kääntäjä siis ei anna tällaisen tapahtua vaan kääntämisen yhteydessä tulee virheviesti, jonka voi nähdä koodiesimerkissä \ref{code:rust:integer-overflow}.

\bigskip

\begin{code}
	\inputminted[linenos=false]{shell}{code/integer-overflow}
	\captionof{listing}{Etumerkittömän kokonaisluvun ylivuoto}
	\label{code:rust:integer-overflow}
\end{code}

\subsection{Muuttumaton data}
Rustissa kaikki arvot ovat oletuksena muuttumattomia (engl.\ immutable). Jos muuttumattoman datan sijasta tarvitsee muuttujia (engl. mutable), voi käyttää avainsanaa ''mut'', esimerkiksi \mintinline{Rust}{let mut name = "Marko"}. Myös lainaukset suoritetaan oletuksena muuttumattomasti ja muutettavan lainauksen voi tehdä samalla avainsanalla, esimerkiksi \mintinline{Rust}{say_hello(&mut name)}

Yleinen konsensus  ohjelmoinnin maailmassa on se, että kaikki arvot, joita ei tarvitse muuttaa, pitäisi nimenomaan määrittää muuttumattomina. Tämä on tuttua kaikille, jotka ovat tutustuneet funktionaalisiin ohjelmointikieliin. Muuttumaton data on myös todella tärkeää rinnakkaisajossa, missä useampi prosessi suorittaa samoja funktioita ja käsittelee samoja arvoja samaan aikaan.

\subsection{Luotettavuus}
\label{sect:luotettavuus}
Rustia kehitettäessä on aina ollut tavoitteena luotettavuus. Tämä tarkoittaa sitä, että ohjelman virheet huomataan jo kääntämisen yhteydessä, eikä vasta suorituksen aikana ohjelman tietyssä tilassa. Tämän mahdollistavat edellä mainitut omistajuusmalli ja vahva tyypitys. Omistajuusmalli varmistaa sen, että ohjelmoija joutuu koodia kirjoittaessaan miettimään arvojen eliniän, joka tekee muistivuodoista harvinaisia. Vahva tyypitys varmistaa taas sen, että data on kaikkialla ohjelmassa yhteensopivaa.

Myös Rustin kääntäjään on panostettu paljon, ja virheiden sattuessa se on todella hyvä työkalu ohjelmoijalle. Se alleviivaa ongelmakohdat ja selittää lyhyesti, mistä ongelma johtuu. Jossain tapauksissa kääntäjä jopa antaa pieniä koodin pätkiä, mistä voi olla apua ongelman ratkaisemisessa [koodiesimerkki \ref{code:rust:help}].

\clearpage

\begin{code}
	\inputminted[linenos=false]{shell}{code/compiler-help}
	\captionof{listing}{Rustin kääntäjä auttaa unohtuneen importin lisäämisessä}
	\label{code:rust:help}
\end{code}

\subsection{Suorituskyky}
Rustin monista luotettavuuteen liittyvistä ominaisuuksista johtuen voisi luulla, että kaikki nämä ominaisuudet vaikuttaisivat negatiivisesti suorituskykyyn. Monissa kielissä onkin runtime-kirjasto \cite{wiki:runtime-library}. Se on kääntäjän jatke, joka lisää käännettyyn binääritiedostoon ajon aikana suoritettavia käskyjä, jotka liittyvät juuri tällaisten luotettavuusominaisuuksien takaamiseen. Nämä käskyt suoritetaan siis itse ohjelman suorituksen yhteydessä, joten ne käyttävät prosessoriaikaa ja muistia aina, kun ohjelma on käynnissä.

Rustissa ei ole runtime-kirjastoa. Kääntäjä tekee kaikki runtime-kirjaston tehtävät kääntämisen yhteydessä. Jos jokin tarkistus ei mene läpi, esimerkiksi omistajuusmallia on käytetty väärin, ohjelman kääntäminen keskeytetään ja kehittäjälle näytetään virheviesti. 

Runtime-kirjaston puuttuminen vapauttaa prosessointiaikaa ja muistia itse ohjelman käyttöön, mikä parantaa suorituskykyä.

\subsection{Turvallinen rinnakkaisajo}
Rinnakkaisajo tarkoittaa sitä, että jotakin ohjelman osaa suoritetaan saman aikaan useassa prosessorin säikeessä. Rinnakkaisajolle sopivissa tehtävissä kuorman jakamisella saavutetaan parempaa suorituskykyä.

Turvallisella rinnakkaisajolla \cite{wiki:thread-safety} tarkoitetaan sitä, että jos rinnakkaisajossa käsitellään tietorakenteita, käsittely suoritetaan turvallisesti. Turvallisuus tarkoittaa sitä, että saavutetaan haluttu tulos eikä ilmene sivuvaikutuksia.

Rust takaa turvallisen rinnakkaisajon luvussa \ref{sect:luotettavuus} mainituilla luotettavuusominaisuuksilla. Tämän asian perinpohjaiseen selittämiseen en ryhdy tässä insinöörityössä. Asiasta kiinnostuneet voivat lukea asiasta lisää Rust Blogista \cite{rust:fearless-concurrency}.

\subsection{Makrot}
\label{chap:macros}
Yksi todella mielenkiintoinen ominaisuus Rustissa on makrot. Se on toiminnallisuus, mikä mahdollistaa metaohjelmoinnin \cite{wiki:metaprogramming}. Metaohjelmoinnissa koodia voi generoida kääntämisen aikana, mikä on erityisen hyödyllistä esimerkiksi silloin, kun ohjelmoija tarvitsee useita toiminnallisuudeltaan samankaltaisia funktioita.

\bigskip

\begin{code}
	\inputminted{Rust}{code/macro.rs}
	\captionof{listing}{Runsassanainen laskin toteutettuna Rustin makrona}
	\label{code:rust:macro}
\end{code}

Makron sisällä suluissa olevat lauseet ovat verrattavissa Rustin ''match''-lauseeseen. Kun makron syöte vastaa jotakin näistä lauseista, hakasulkujen sisällä oleva koodi generoidaan. Koodiesimerkin \ref{code:rust:macro} makrossa käytetty ''println!()'' on myös itsessään makro, joka tulee Rustin ''std'' -kirjaston mukana.

\bigskip

\begin{code}
	\inputminted{Rust}{code/println.rs}
	\captionof{listing}{Rustin sisäänrakennettu println!() makro \cite{rust:println}}
	\label{code:rust:println}
\end{code}

Metaohjelmointi avaa aivan uudenlaisia mahdollisuuksia sille, mitä ohjelmointikielellä voi tehdä. Makroilla voi toteuttaa vaikka kokonaisen ohjelmointikielen \cite{rust:macro-lisp}.

\subsection{Dokumentaatio ja yhteisö}
Rust on tunnettu todella laajasta dokumentaatiostaan ja vahvasta yhteisöstään. Molemmista on paljon apua varsinkin aloittelijoille.

Aloitin itsekin opiskelemaan Rustia vain hieman ennen tämän insinöörityön alkua. Yhteisöstä oli monesti apua projektin aikana vastaan tulleissa ongelmissa.

\subsection{Paketinhallinta}
\label{sect:paketinhallinta}
Rustin paketinhallinta on toteutettu Cargo-nimisellä ohjelmalla. Sitä voi käyttää koko ohjelmiston elinkaaren ajan aina projektin luomisesta sen julkaisemiseen. Cargon käsittelemiä paketteja kutsutaan laatikoiksi (eng. crate), jotka julkaistaan crates.io-pakettirekisterissä \cite{rust:cratesio}. Laatikot voivat myös olla riippuvaisia toisista laatikoista. Laatikon tiedot ja riippuvuudet määritetään Cargo.toml-tiedostossa [koodiesimerkki \ref{code:rust:cargo-toml}].

\clearpage

\begin{code}
	\inputminted{TOML}{code/Cargo.toml}
	\captionof{listing}{Projektin palvelinpuolen Cargo.toml}
	\label{code:rust:cargo-toml}
\end{code}

Cargoon on saatavilla myös useita liitännäisiä, esimerkiksi cargo-watch, joka suorittaa halutun toiminnon aina, kun projektin sisällä tapahtuu muutoksia sekä tässäkin insinöörityössä käytetty cargo-web, joka helpottaa WebAssembly-ohjelmien kehittämistä.

\section{WebAssembly}
WebAssembly \cite{webassembly:home} on kehitteillä oleva asiakaspuolen ohjelmointikieli. Sitä on suunniteltu JavaScriptin seuraajaksi ja sen suurimpana etuna verrattuna JavaScriptiin on huomattavasti matalamman tason esitysmuoto, minkä ansiosta se on suorituskykyisempi.

Kehittäjän ei ole tarkoitus kirjoittaa WebAssemblya itse, vaan käyttää työkaluja, joilla olemassa olevia ohjelmointikieliä voi kääntää WebAssemblyksi. Rust on tästä hyvä esimerkki, sillä WebAssembly on yksi sen kääntäjän natiiveista ''targeteista'', samalla tavalla kuin vaikka x86-prosessorit.

\clearpage

WebAssembly on ensisijaisesti binääriformaatti, mutta sen voi muuntaa myös tekstiformaatiksi, jonka nimi on WebAssembly text \cite{webassembly:text}. WebAssembly text käyttää syntaksissaan S-lausekkeita \cite{s-expression}. Se on notaatio puurakenteiselle datalle, joka on kehitetty Lisp-ohjelmointikieltä varten, joten WebAssembly text muistuttaa syntaksiltaan hyvin paljon Lispiä. WebAssembly textiä käytetään tilanteissa, joissa ihmisen täytyy ymmärtää, mitä koodissa tapahtuu. Tätä hyödynnetään esimerkiksi WebAssemblyn sisäisessä kehityksessä ja web-ohjelmistojen debuggereissa.

\bigskip

\begin{code}
	\inputminted{Lisp}{code/plus.wat}
	\captionof{listing}{Lukujen yhteenlaskufunktio WebAssembly text -formaatissa}
	\label{code:webassembly:plus}
\end{code}

\bigskip

\begin{code}
	\inputminted{Rust}{code/plus.rs}
	\captionof{listing}{Lukujen yhteenlaskufunktio Rustilla}
	\label{code:rust:plus}
\end{code}

Koodiesimerkeistä \ref{code:webassembly:plus} ja \ref{code:rust:plus} voi nähdä saman koodin sekä Rustilla toteutettuna että WebAssembly text -formaatissa. Esimerkeistä huomaa hyvin WebAssemblyn matalemman esitysmuodon käytännössä.
